package tutorial.android.com.android14_settings_example;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {
    public static final String CHECKBOX_SETTING_KEY = "pref_checkBox";
    public static final String LIST_SETTING_KEY = "pref_listPref";
    public static final String PREFERENCE_1_SETTING_KEY = "pref_key_item_1";
    public static final String PREFERENCE_2_SETTING_KEY = "pref_key_item_2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
    public static class SettingsFragment extends PreferenceFragment {
        SharedPreferences.OnSharedPreferenceChangeListener listener;
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    Toast.makeText(getActivity(), "preferences changed", Toast.LENGTH_SHORT).show();

                    if(key.equals(LIST_SETTING_KEY)){
                        Preference pref = findPreference(LIST_SETTING_KEY);
                        if (pref instanceof ListPreference) {
                            ListPreference etp = (ListPreference) pref;
                            pref.setSummary(etp.getEntry());
                        }
                    }
                }
            };

            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);
        }
    }
}

