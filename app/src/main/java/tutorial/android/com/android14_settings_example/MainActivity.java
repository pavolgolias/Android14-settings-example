package tutorial.android.com.android14_settings_example;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

//settings application demo
//for more info check http://developer.android.com/guide/topics/ui/settings.html

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        showSettingsValues();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showSettingsValues() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBoxValue = sharedPref.getBoolean(SettingsActivity.CHECKBOX_SETTING_KEY, false);
        String listValue = sharedPref.getString(SettingsActivity.LIST_SETTING_KEY, "no value in list");
        String preference1Value = sharedPref.getString(SettingsActivity.PREFERENCE_1_SETTING_KEY, "no value in preference 1");
        String preference2Value = sharedPref.getString(SettingsActivity.PREFERENCE_2_SETTING_KEY, "no value in preference 2");


        ((TextView) findViewById(R.id.checkBox_value)).setText(String.valueOf(checkBoxValue));
        ((TextView) findViewById(R.id.prefrenceList_value)).setText(listValue);
        ((TextView) findViewById(R.id.preference_1_value)).setText(preference1Value);
        ((TextView) findViewById(R.id.preference_2_value)).setText(preference2Value);
    }
}
